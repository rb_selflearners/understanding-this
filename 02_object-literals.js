// Implicit binding of this keyword

var myMethodA = function() {
    console.log();
    console.log("this ==> " + this);
    if ( this === global)
        console.log("\tthis === global");
    else if ( this === myObject)
        console.log("\tthis === myObject");
    else
        console.log("\tthis !== global && this !== Object");
};


myMethodA(); // here this === global

var myObject = {
    myMethod: myMethodA
};

myMethodA(); // here this === global

myObject.myMethod();  // here this === myObject