var myMethod = function() {
    console.log();
    console.log("this ==> " + this);
    if ( this === global)
        console.log("\tthis === global");
        else if ( this === obj1)
        console.log("\tthis === obj1");
        else if ( this === obj2)
        console.log("\tthis === obj2");
    else
        console.log("\tthis !== global && this !== Object");
};

var obj1 = {
  a: 2,
  myMethod: myMethod
};

var obj2 = {
  a: 3,
  myMethod: myMethod
};

obj1.myMethod(); // 2
obj2.myMethod(); // 3

// Explicit binding takes precedence over implicit binding,
// which means you should ask first if explicit binding 
// applies before checking for implicit binding.

obj1.myMethod.call( obj2 ); // this === obj2, because explicitely bound to obj2
obj2.myMethod.call( obj1 ); // this === obj1, because explicitely bound to obj1

// Hardbinding takes precedence over explicit binding
console.log("");
console.log("");
myMethod = myMethod.bind(obj1);

myMethod(); // this === obj1, because hard bound to obj1

myMethod.call(obj2); // this === obj1, because hard bound to obj1
myMethod.apply(obj2); // this === obj1, because hard bound to obj1

console.log(""); 
console.log("");
myMethod = myMethod.bind(obj2);  // No change in binding, hard binding cant be overwritten!

myMethod(); // this === obj1, becuase no overwritting of hard binding!

myMethod.call(obj1); // this === obj1, becuase no overwritting of hard binding!
myMethod.apply(obj1); // this === obj1, becuase no overwritting of hard binding!
myMethod.call(obj2); // this === obj1, becuase no overwritting of hard binding!
myMethod.apply(obj2); // this === obj1, becuase no overwritting of hard binding!



