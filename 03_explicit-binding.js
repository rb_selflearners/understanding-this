var myMethod = function() {
    console.log();
    console.log("this ==> " + this);
    if ( this === global)
        console.log("\tthis === global");
    else if ( this === myObject)
        console.log("\tthis === myObject");
    else
        console.log("\tthis !== global && this !== Object");
};

var myObject = {
  myMethod: myMethod
};

myMethod() // this === window
myMethod.call(myObject) // this === myObject
myMethod.apply(myObject) // this === myObject